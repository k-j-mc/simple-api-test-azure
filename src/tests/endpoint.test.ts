import app from "../app";
import request from "supertest";

const { VERSION } = process.env;

describe("Sample Test", () => {
	it("should test that true === true", () => {
		expect(true).toBe(true);
	});
});

describe("GET / - a simple api endpoint", () => {
	test("Test endpoint", async () => {
		const res = await request(app).get("/");

		expect(res.status).toBe(200);
		expect(res.text).toEqual(`Simple API V${VERSION}`);
	});
	test("Test endpoint get json", async () => {
		const res = await request(app).get("/test");

		expect(res.status).toBe(200);
		expect(res.body.message).toEqual("test");
	});
});

describe("POST / - a simple api endpoint", () => {
	test("Test endpoint post json", async () => {
		const obj = {
			message: "testing post enpoint",
		};

		const res = await request(app).post("/test-json").send(obj);

		expect(res.status).toBe(201);
		expect(res.body).toEqual(obj);
	});
});
