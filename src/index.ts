import app from "./app";
import dotenv from "dotenv";

dotenv.config({ path: "../env" });

const { PORT } = process.env;

app.listen(PORT, () => {
	console.log("Simple API Server listening on port:" + PORT);
});
