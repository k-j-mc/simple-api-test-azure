import { Request, Response, Router } from "express";

const router = Router();

const { VERSION } = process.env;

router.get("/", (request: Request, response: Response) => {
	response.status(200).send("Simple API V" + VERSION);
});

router.get("/test", (request: Request, response: Response) => {
	response.status(200).json({ message: "test" });
});

router.post("/test-json", (request: Request, response: Response) => {
	const { body } = request;
	response.status(201).json(body);
});

router.post("/version", (request: Request, response: Response) => {
	response.status(200).json({ version: VERSION });
});

export default router;
