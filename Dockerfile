FROM node:latest

WORKDIR /app

EXPOSE 5000

COPY package*.json ./
COPY tsconfig.json ./

RUN npm install --silent

ARG PORT=5000
ARG VERSION=2.0

ENV PORT=$PORT
ENV VERSION=$VERSION

COPY . ./

CMD ["npm", "run", "dev"]